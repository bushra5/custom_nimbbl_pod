//
//  NimbblUpiSupport.swift
//  custom_nimbbl_pod
//
//  Created by LogicLoop_LL0115 on 22/05/23.
//

import Foundation
import UIKit

struct NimbblUpiSupport {
    
    let name: String
    let scheme: String
    
    var url: URL {
        
        let urlString = String(format: "%@://", self.scheme)
        let url = URL(string: urlString)!
        return url
    }
}


extension NimbblUpiSupport {
    
    static func getAllUPIApps() -> [NimbblUpiSupport] {
        
        return [NimbblUpiSupport(name: "GPay", scheme: "gpay"),
                NimbblUpiSupport(name: "Phone Pe", scheme: "phonepe"),
                NimbblUpiSupport(name: "Paytm", scheme: "paytmmp")]
    }
    
    static func getInstalledApps() -> [NimbblUpiSupport] {
        
        let apps: [NimbblUpiSupport] = getAllUPIApps().filter {
            
            if UIApplication.shared.canOpenURL($0.url) {
                return true
            } else {
                return false
            }
        }
        
        return apps
    }
}
