# custom_nimbbl_pod

[![CI Status](https://img.shields.io/travis/LL0115/custom_nimbbl_pod.svg?style=flat)](https://travis-ci.org/LL0115/custom_nimbbl_pod)
[![Version](https://img.shields.io/cocoapods/v/custom_nimbbl_pod.svg?style=flat)](https://cocoapods.org/pods/custom_nimbbl_pod)
[![License](https://img.shields.io/cocoapods/l/custom_nimbbl_pod.svg?style=flat)](https://cocoapods.org/pods/custom_nimbbl_pod)
[![Platform](https://img.shields.io/cocoapods/p/custom_nimbbl_pod.svg?style=flat)](https://cocoapods.org/pods/custom_nimbbl_pod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

custom_nimbbl_pod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'custom_nimbbl_pod'
```

## Author

LL0115, bushra@logicloop.io

## License

custom_nimbbl_pod is available under the MIT license. See the LICENSE file for more info.
